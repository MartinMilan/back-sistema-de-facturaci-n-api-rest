package com.carlosmartin.springboot.backend.apirest.auth;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.oauth2.config.annotation.configurers.ClientDetailsServiceConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configuration.AuthorizationServerConfigurerAdapter;
import org.springframework.security.oauth2.config.annotation.web.configuration.EnableAuthorizationServer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerEndpointsConfigurer;
import org.springframework.security.oauth2.config.annotation.web.configurers.AuthorizationServerSecurityConfigurer;
import org.springframework.security.oauth2.provider.token.TokenEnhancerChain;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;

import java.util.Arrays;


/**
 * REVISAR nueva versión OAuth 2.4.0 ya que en la misma todas las clases quedan deprecated, esto incluye
 * @EnableAuthorizationServer y AuthorizationServerConfigurerAdapter
 */
@Configuration
@EnableAuthorizationServer
public class AuthorizationServerConfig extends AuthorizationServerConfigurerAdapter {

    @Autowired
    private BCryptPasswordEncoder passwordEncoder;

    @Autowired
    @Qualifier("authenticationManager")
    private AuthenticationManager authenticationManager;

    @Autowired
    private InfoAdicionalToken infoAdicionalToken;

    /**
     * Acá se configuran los permisos de nuestros endpoints, nuestras rutas de acceso, hay 2 endpoints en el
     * authorizationServer, uno para autenticarnos, iniciar sesión y se encarga de generar el token y enviarlo al usuario
     * y esa ruta debe ser completamente pública y otro para validar el token cada vez que se solicite un recurso
     */
    @Override
    public void configure(AuthorizationServerSecurityConfigurer security) throws Exception {
        security.tokenKeyAccess("permitAll()")// Damos permiso a cualquier usuario, anónimo o no
                .checkTokenAccess("isAuthenticated()"); // Solo pueden acceder a esta ruta los usuarios autenticados
    }

    /**
     * Con este método vamos a registrar a nuestro cliente angular, si hubieran mas clientes deberiamos registrarlos acá
     */
    @Override
    public void configure(ClientDetailsServiceConfigurer clients) throws Exception {
        clients.inMemory().withClient("angularapp") // Usuario
                .secret(passwordEncoder.encode("12345")) // Secreto
                .scopes("read", "write") // Permisos de la aplicación
                .authorizedGrantTypes("password", "refresh_token") // Concesión o tipo de autenticación (renovacion de token automatica)
                .accessTokenValiditySeconds(3600)//Tiempo de caducidad del token
                .refreshTokenValiditySeconds(3600);// Cada cuantos segundos se renueva el token (para no volver a iniciar sesión)
    }

    /**
     * End Point a través del cual se realiza la autenticación y si todo sale bien devuelve el token
     * @param endpoints
     * @throws Exception
     */
    @Override
    public void configure(AuthorizationServerEndpointsConfigurer endpoints) throws Exception {
        TokenEnhancerChain tokenEnhancerChain = new TokenEnhancerChain();
        tokenEnhancerChain.setTokenEnhancers(Arrays.asList(infoAdicionalToken, jwtAccessTokenConverter()));

        endpoints.authenticationManager(authenticationManager)
                .accessTokenConverter(jwtAccessTokenConverter())//Se encarga de gestionar la informacion del token (claims)
                .tokenEnhancer(tokenEnhancerChain);
    }

    @Bean
    public JwtAccessTokenConverter jwtAccessTokenConverter(){
        JwtAccessTokenConverter jwtAccessTokenConverter = new JwtAccessTokenConverter();
        jwtAccessTokenConverter.setSigningKey(JwtConfig.RSA_PRIVADA);
        jwtAccessTokenConverter.setVerifierKey(JwtConfig.RSA_PUBLICA);
        return jwtAccessTokenConverter;
    }
}
