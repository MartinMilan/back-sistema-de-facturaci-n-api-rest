package com.carlosmartin.springboot.backend.apirest.models.services;

import com.carlosmartin.springboot.backend.apirest.models.entity.Usuario;

public interface IUsuarioService {
    public Usuario findByUsername(String username);
}
