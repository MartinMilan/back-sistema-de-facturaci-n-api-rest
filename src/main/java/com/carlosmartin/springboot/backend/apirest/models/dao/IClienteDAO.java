package com.carlosmartin.springboot.backend.apirest.models.dao;

import com.carlosmartin.springboot.backend.apirest.models.entity.Cliente;
import com.carlosmartin.springboot.backend.apirest.models.entity.Region;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import java.util.List;

/**
 * Vamos a agregar paginación en el front end para que el listado de clientes se muestre por páginas
 * para ello en ves de heredar de CrudRepository, heredamos de JpaRepository
 */
public interface IClienteDAO extends JpaRepository<Cliente, Long> {
    @Query("from Region")
    public List<Region> findAllRegiones();
}
