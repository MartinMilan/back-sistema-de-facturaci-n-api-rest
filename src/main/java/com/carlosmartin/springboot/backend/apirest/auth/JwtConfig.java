package com.carlosmartin.springboot.backend.apirest.auth;

public class JwtConfig {
    public static final String LLAVE_SECRETA = "alguna.clave.secreta.12345";

    public static final String RSA_PRIVADA = "-----BEGIN RSA PRIVATE KEY-----\n" +
            "MIIEowIBAAKCAQEAus1BW0m4kHhgOpiXOFD3beNi129SbhO8Cb3Q0OSlvrx78nED\n" +
            "lZC4sR+k4VbbTL7uSXCSJx014RgZ77kwWmOzt3SgVvXfo4693O+7trUy+jINws7i\n" +
            "XoOSL8QCZ9v5Eu95f1iAmzFiQIVdSzZhmxD0v8pOm9dJDWTlf4oYKC2z2bD1eYx/\n" +
            "A35hc/R40GX42xYY0lG5Gol37iACqi0WNPHGDtsWCX6aGUPhkvhOBvOfBKfDLO+y\n" +
            "wpiAT6PBDdH5hoSUQuIa7x19b91Hp7JoMwT31Y5B8em0zVqpOBo+bOTx1iSlzwj/\n" +
            "MqJjUDP1t6zXmSmxDuZbJ7rC8GyBnM0UOmGfswIDAQABAoIBAQCjQJycpkn5YGpg\n" +
            "61hFLGWBENciBjUY5IBSVyQ2W40Kq3j7QY9rV+ZBTbFwCdNwN/DSF+Jy8pFS3hhU\n" +
            "rBi49idq7i7usMS5EReXDaCUpbkedT4XgWzOQtgwjHG5yMHdrZIWHAeRGdCPKL6G\n" +
            "68jTlzb+olSe18ANR59WHh7MhlyGoyR5XBuqhf9+CpcS0iHKCEpDFTZV1P5I76Q3\n" +
            "s26xEUwR3pkDdYg8x/1spkV/IAy1/d86faWRUU4JO1wlCv8fBvStraVq04bslsG5\n" +
            "xBOztbOjoZ0d2d8ZtQ6r7QPMMm2vjRIHOcGS1HGJr9CmBWSzZe9zw5ViVeaQFUqf\n" +
            "65caTRB5AoGBAOjWwEsz5myVioma97gUXneraCrnv3qSvOiO2eoCkQJMt/KTqVTC\n" +
            "AeiE3GHrL+bMRnI6R0VHCPvfOlTrMN6Z2SxAYr5gjGnpFqoZiIkYuLTeC+YpZ+A3\n" +
            "Sjzx8PBsHi3KhrIx3LpQfw22+s3EIpS0kIUWp4tMSfdF2uuIdcqzazk/AoGBAM1i\n" +
            "Kw6235UzL2GcxdnratgFGgbv7imCNgCcOP34I2iznMIzPLRn+ADkXSAGu3lLzqJe\n" +
            "wLl4uRMfLalL3aViuNNWQxQqA07/0fr7fOpvLM1y7c0q5wKqdDzflFmUGZpEfFG8\n" +
            "HOc8ny/mmt/0aDHcKUY+ZYXl5NU9iU9K3Y0lDuiNAoGAVquRmqywCh1EG2K28SuL\n" +
            "2onQVaA8U1kJvptmHEhmR+/czQwQDP/GjBHgowZTp1rEokJ1QyFlA7SdVvzRDRtB\n" +
            "jspMt0bjHY1pJVopy7aLErBQx5UEGJ8cIEGDCUuFlE/+AZiz7BqIoMdK5osqwCXS\n" +
            "YegD58M3CTdUfTYnepN8xysCgYA/+keNYtwM/w/QaWSceKAkwJoV9SGMXppx4DhE\n" +
            "rMpi8ChxukI7tHNl271Cn7ZAl6O7oqaIYBnO4hR4O0DFJxaKBsIIgvna3wfLFhIY\n" +
            "eR9MtHNtSOMDSpPA7qioquC61ICheWbLhW/VHRG7l8fpWDqnPgKkDjQVVQDz6U9Q\n" +
            "i220dQKBgE5xLGwKUdjFL0rMrE465BPnYOE1HrnREuV03YQ38CYtq52pv5e6xHRX\n" +
            "SnzgV1786v/HT4/81jdBNd1IqM/2pdYgW/JiXZ37zVXctibFDhS9rbQc6DecSs2f\n" +
            "umjp4LyInW6I0JTgIeyh78OPOWxCqfUmw9XzZLrcWp1qO9T3nK4V\n" +
            "-----END RSA PRIVATE KEY-----";
    public static final String RSA_PUBLICA = "-----BEGIN PUBLIC KEY-----\n" +
            "MIIBIjANBgkqhkiG9w0BAQEFAAOCAQ8AMIIBCgKCAQEAus1BW0m4kHhgOpiXOFD3\n" +
            "beNi129SbhO8Cb3Q0OSlvrx78nEDlZC4sR+k4VbbTL7uSXCSJx014RgZ77kwWmOz\n" +
            "t3SgVvXfo4693O+7trUy+jINws7iXoOSL8QCZ9v5Eu95f1iAmzFiQIVdSzZhmxD0\n" +
            "v8pOm9dJDWTlf4oYKC2z2bD1eYx/A35hc/R40GX42xYY0lG5Gol37iACqi0WNPHG\n" +
            "DtsWCX6aGUPhkvhOBvOfBKfDLO+ywpiAT6PBDdH5hoSUQuIa7x19b91Hp7JoMwT3\n" +
            "1Y5B8em0zVqpOBo+bOTx1iSlzwj/MqJjUDP1t6zXmSmxDuZbJ7rC8GyBnM0UOmGf\n" +
            "swIDAQAB\n" +
            "-----END PUBLIC KEY-----";
}
