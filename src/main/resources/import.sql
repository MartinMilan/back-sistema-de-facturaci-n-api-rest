Insert INTO regiones(id, nombre) VALUES (1, 'Sudamérica');
Insert INTO regiones(id, nombre) VALUES (2, 'Centroamérica');
Insert INTO regiones(id, nombre) VALUES (3, 'Norteamérica');
Insert INTO regiones(id, nombre) VALUES (4, 'Europa');
Insert INTO regiones(id, nombre) VALUES (5, 'Asia');
Insert INTO regiones(id, nombre) VALUES (6, 'Africa');
Insert INTO regiones(id, nombre) VALUES (7, 'Oceanía');
Insert INTO regiones(id, nombre) VALUES (8, 'Antártida');
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Martin', 'Milan', 'carlos000@correo.com', '2019-12-26', 1);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Julio', 'Zapata', 'pepejul@correo.com', '2019-11-12', 2);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Florencia', 'Carbonatti', 'flor.carbon@hipermail.com', '2019-12-19', 1);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('alejandra', 'Acosta', 'ale.cos@hipermail.com', '2018-8-12', 1);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Pamela', 'Castro', 'castro.pame@correo.com', '2019-10-21', 2);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Javier', 'Ruiz', 'javier_09@correo.com', '2019-11-12', 3);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Odilia', 'Fernandez', 'odilia.fernandez@hipermail.com', '2019-1-18', 5);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Zelma', 'Ledezma', 'zlemi.87@correo.com', '2019-10-2', 3);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Quirina', 'Torres', 'quiri.torres2000@correo.com', '2019-2-13', 4);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Isabel', 'Pérez', 'isabelita12@correo.com', '2018-6-6', 4);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Kalena', 'Cocinero', 'kalekale_09@correo.com', '2017-9-22', 1);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Kenaz', 'Carrizo', 'kenaz.98@correo.com', '2018-10-8', 5);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Victoria', 'Espinoza', 'vicky.78@correo.com', '2017-5-23', 2);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Matias', 'Abbatelli', 'mati.abba.12@correo.com', '2019-10-1', 1);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('William', 'Peñalosa', 'willis.peña93@hipermail.com', '2016-12-29', 6);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Teresa', 'Garcia', 'teresita72@hipermail.com', '2018-4-10', 4);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Yamila', 'Lima', 'yami.lima88@correo.com', '2018-12-31', 1);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Zoé', 'Farías', 'zoe98@correo.com', '2019-7-7', 6);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Xenia', 'Vega', 'vega.xenia@correo.com', '2019-2-2', 7);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Andrea', 'Cáceres', 'andrea.89@hipermail.com', '2017-9-28', 5);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Lázaro', 'Frankestein', 'lazaro88@correo.com', '2019-3-10', 1);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Ianina', 'Acosta', 'ianicosta_90@correo.com', '2018-07-28', 7);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Elisa', 'Correa', 'elisa2017@correo.com', '2018-8-4', 8);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Ottone', 'Sislack', 'otto91@hipermail.com', '2019-4-29', 4);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Kensel', 'Williams', 'kensel.w78@correo.com', '2017-10-17', 1);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Alma', 'Sosa', 'alma2001@hipermail.com', '2019-4-12', 8);
Insert INTO clientes (nombre, apellido, email, create_at, region_id) values ('Galvin', 'Smith', 'galvin.smith90@hipermail.com', '2019-5-11', 2);

/*Creamos algunos usuarios*/
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES('martin', '$2a$10$w3Px83XMslPkm8/k9JTk6.kvnW.12GLfex7i2GL107Ot9iHirXsU2', 1, 'Carlos Martin', 'Milan', 'carlos.milan@email.com');
INSERT INTO `usuarios` (username, password, enabled, nombre, apellido, email) VALUES('admin', '$2a$10$VJuzqmUsjn87NquUXWgJi..8ycx5Ppm8vdV7.wbUJKz62mVyH51Wu', 1, 'John', 'Wick', 'elhombredelabolsa@johnmail.com');

INSERT INTO `roles`(nombre) VALUES('ROLE_USER');
INSERT INTO `roles`(nombre) VALUES('ROLE_ADMIN');

INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES(1,1);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES(2,2);
INSERT INTO `usuarios_roles` (usuario_id, role_id) VALUES(2,1);


INSERT INTO productos(nombre, precio, create_at) values ("Monitor LG LED 22MK400HB", 14999, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Camara Sony DSC-WX500", 29999, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Notebook Gamer ASUS FX504GMEN476T", 89499, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Samsung Galaxy S10 G973 AZUL", 77499, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Sony PlayStation CUH2215B 1 TB", 34999, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Xbox ONE S 1TB", 29999, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Samsung Galaxy T510 101", 21899, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Samsung Smart TV 32 HD UN32J4290AG", 17499, NOW());
INSERT INTO productos(nombre, precio, create_at) values ("Logitech G433 71 BLACK", 8559, NOW());

/*Vamos a crear algunas facturas*/
INSERT INTO facturas(descripcion, observacion, cliente_id, create_at) VALUES ('Factura equipos de oficina', null, 1, NOW());
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES (1, 1, 1);
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES (2, 1, 4);
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES (1, 1, 5);
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES (1, 1, 7);

INSERT INTO facturas(descripcion, observacion, cliente_id, create_at) VALUES ('Factura equipos informaticos', null, 1, NOW());
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES (2, 2, 3);
INSERT INTO facturas_items(cantidad, factura_id, producto_id) VALUES (2, 2, 9);
